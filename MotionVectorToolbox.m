%{
Author: Wen, Hao-KAi
Date: 2013/12/15
Purpose:
It extracts a set of motion vector of a clip.
%}
classdef MotionVectorToolbox < handle
    properties
        video;
        T = 0; % threshold of no motion direction
        blockSize;
        motion; % motion vectors of each frame
        frameLatency; % i frame and i -latency frame motion
        histogram; % motion histogram of each frame
        AlgName;
    end
    methods
        % constructor
        function MVT = MotionVectorToolbox(video)
            MVT.video = video;
            MVT.histogram = zeros(3, 3, video.frameNum);
        end
        %{
        % input
        %     blockSize: must be odd number.
        %     AlgName: the algorithm used to find motion vector
        % output:
        %}
        function motion = cal(MVT, blockSize, AlgName, frameLatency)
            %{
            % defualt block Size is 17 by 17
            % default Algorithm is BlockMatcher
            %}
            if nargin < 2
                MVT.blockSize = 17;
            else
                MVT.blockSize = blockSize;
            end    
            if nargin < 3
                MVT.AlgName = 'BlockMatcher';
                disp(MVT.AlgName);
            else
                MVT.AlgName = 'BlockMatcher';
            end
            if nargin < 4
                MVT.frameLatency = 1;
            else
                MVT.frameLatency = frameLatency;
            end
            if strcmp(MVT.AlgName,'BlockMatcher')
                MVT.blockmatcher();
                motion = MVT.motion;
            else
                error(strcat(AlgName,' is not supported.'));
            end
        end
        function blockmatcher(MVT)
            tic
            disp('start blockmatcher');
            preNum = 100;
            for i = 1:MVT.video.frameNum
                % preload preNum frames into memory
                % release used frames out of memory
                
                if mod(i,preNum) == 1
                    for j = i:i+preNum-1
                        if j > MVT.video.frameNum
                            break;
                        else
                            MVT.video.loadFrame(j);
                        end
                    end
                    if i-preNum >= 1
                        for j = i-preNum:i-1
                            MVT.video.clearFrame(j);
                        end
                    end
                end
                % Block Matcher Algorithm
                if i <= MVT.video.frameNum - MVT.frameLatency
                    img1 = im2double(rgb2gray(MVT.video.getFrame(i)));
                    img2 = im2double(rgb2gray(MVT.video.getFrame(i+MVT.frameLatency)));
                    hbm = vision.BlockMatcher( ...
                        'ReferenceFrameSource','Input port',...
                        'BlockSize',[MVT.blockSize MVT.blockSize]);
                    hbm.OutputValue = ...
                        'Horizontal and vertical components in complex form';
                    hbm.MaximumDisplacement = [10*MVT.blockSize, 10*MVT.blockSize];
                    %halphablend = vision.AlphaBlender;
                    MVT.motion{i} = step(hbm, img1, img2); 
                    %img12 = step(halphablend, img2, img1);

                    %[X, Y] = meshgrid(1:block_size_y:size(img1, 2), 1:block_size_x:size(img1, 1));
                    %imshow(img12); hold on;
                    %quiver(X(:), Y(:), real(motion(:)), imag(motion(:)),0); hold
                    %off;
                else
                    % exceed frameNum
                end
                % end Block Matcher Algorithm
            end            
            % release the remaining frame out of memory in the end
            remainNum = MVT.video.frameNum - ...
                mod(MVT.video.frameNum, preNum) + 1;
            for i = remainNum : MVT.video.frameNum
                MVT.video.clearFrame(i);
            end
            toc
        end
        function m = getMotion(MVT, motionIndex)
            m = MVT.motion{motionIndex};
        end
        %{
        % save image with motion vector
        % input
        % frameIndex
        % destination: like 'clips\testclip_motion\'
        %}
        function saveImg(MVT, frameIndex, destination)
            h = figure('visible','off');
            %{
            % plot Frame Image
            %}
            img1 = im2double(rgb2gray(MVT.video.getFrame(frameIndex)));
            bSize = MVT.blockSize;
            [X, Y] = meshgrid(1:bSize:size(img1, 2), 1:bSize:size(img1, 1));
            imshow(img1); hold on;
            m = MVT.getMotion(frameIndex);
            %{
            % plot Motion Vection of each block
            %}
            quiver(X(:), Y(:), real(m(:)), imag(m(:)),0); hold off;
            path = fullfile(destination, int2str(frameIndex));
            disp(path);
            %{
            % If the desination folder is not exist then create one.
            %}
            if ~exist(fullfile(destination), 'dir')
              mkdir(destination);  
            end
            saveas(h, path, 'jpeg');
            MVT.video.clearFrame(frameIndex);
            close(h);
        end
        function saveAllImg(MVT, destination)
            for i = 1:MVT.video.frameNum - MVT.frameLatency
                tic
                MVT.saveImg(i,destination);
                toc
            end
        end
        %{
        % input
        % frameNum: the histogram of frame Num
        %
        % output
        % h: a nine direction counter of the direction of each block
        %     ul u  ru
        %     l  no r
        %     ld d  dr
        up-left
        %}
        function h = hist(MVT, frameNum, T)
            % motion vector threshold
            MVT.T = T;
            motionDim = size(MVT.motion{frameNum});
            % The angle lies between +- pi
            theta = angle(MVT.motion{frameNum});
            magnitude = abs(MVT.motion{frameNum});
            % h is a counter of motion vector 9 directions.
            h = zeros(3, 3);
            %{
            % Any motion vector with small magnitude is considered as
            % no motion.
            % theta - pi/2 is down direction
            % theta 0 is right direction
            % % theta pi/2 is up direction
            %}
            for i = 1:motionDim(1)
                for j = 1:motionDim(2)
                    t = theta(i, j);
                    if  magnitude(i) == 0
                        %{
                        % Zero magnitude motion is consided as no motion
                        % in all circumstances.
                        %}
                        h(2, 2) = h(2, 2) + 1;
                    elseif magnitude(i) >= T
                        %{
                        % +-pi to -3/4*pi to -1/2*pi to -1/4*pi to
                        % 0 to +1/4*pi to +1/2*pi to +3/4*pi
                        %}
                        % left
                        if (pi - pi/8 < t && t <= pi) ||...
                                (-pi <= t && t <= -pi + pi/8)
                            h(2, 1) = h(2, 1) + 1;
                        % left down
                        elseif -3/4*pi - pi/8 < t && t <= -3/4*pi + pi/8
                            h(3, 1) = h(3, 1) + 1;
                        % down
                        elseif -pi/2 - pi/8 < t && t <= -pi/2 + pi/8
                            h(3, 2) = h(3, 2) + 1;
                        % down right
                        elseif -pi/4 - pi/8 < t && t <= -pi/4 + pi/8
                            h(3, 3) = h(3, 3) + 1;
                        % right
                        elseif 0 - pi/8 < t && t <= 0 + pi/8
                            h(2, 3) = h(2, 3) + 1;
                        % right up
                        elseif pi/4 - pi/8 < t && t <= pi/4 + pi/8
                            h(1, 3) = h(1, 3) + 1;
                        % up
                        elseif pi/2 - pi/8 < t && t <= pi/2 + pi/8
                            h(1, 2) = h(1, 2) + 1;
                        % up left
                        elseif 3/4*pi - pi/8 < t && t <= 3/4*pi + pi/8
                            h(1, 1) = h(1, 1) + 1;
                        else
                            err('Motion Vector theta error');
                        end
                    else
                        h(2, 2) = h(2, 2) + 1;
                    end
                end
            end
            MVT.histogram(:, :, frameNum) = h;
        end
   end
end