%{
Author: Wen, Hao-KAi
Date: 2013/12/16
Purpose: test Video
%}
disp('start testing Video');
v = Video('clips\testclip\','jpeg');
v.loadFrame(2);
f = v.getFrame(2);
v2 = Video('clips\testclip\');
f2 = v2.getFrame(100);
figure, imshow(f);
figure, imshow(f2);
v.clearFrame(2);
clear v;
clear v2;
clear f;
clear f2;
disp('end testing Video');