%{
Author: Wen, Hao-KAi
Date: 2013/12/16
Purpose:
test Renamer
%}
%{
% create testing file and folder
% dir: test_renamer_temp
% files:
%     3.txt, 5.txt, 6.txt, 9.txt, 10.txt, 13.txt, 14.txt
%}
disp('start testing Renamer');
mkdir test_renamer_temp;
fclose(fopen('test_renamer_temp/3.txt', 'w'));
fclose(fopen('test_renamer_temp/5.txt', 'w'));
fclose(fopen('test_renamer_temp/6.txt', 'w'));
fclose(fopen('test_renamer_temp/9.txt', 'w'));
fclose(fopen('test_renamer_temp/10.txt', 'w'));
fclose(fopen('test_renamer_temp/13.txt', 'w'));
fclose(fopen('test_renamer_temp/14.txt', 'w'));
disp('files in test_renamer_temp/');
dir test_renamer_temp/;
disp('rename files in test_renamer_temp/');
Renamer.rename('test_renamer_temp/','txt');
disp('files in test_renamer_temp/');
dir test_renamer_temp/;
disp('remove test_renamer_temp/');
rmdir('test_renamer_temp', 's');
disp('end testing Renamer');