%{
Author: Wen, Hao-KAi
Date: 2013/12/15
Purpose:
Video Converter converts images to video and vice versa.
%}
classdef VideoConverter
    properties
    end
    methods
        % constructor
        function VC = VideoConverter()                
        end
    end
    methods(Static)
        %{
        input
        dirName: image sequence dir name
        typeName: type name, like jpeg
        VideoName: like video.avi
        FrameRate: video frame rate
        %}
        function img2video(dirName, typeName, VideoName, FrameRate)
            %{
            % read all image Name in the folder
            %}
            % file_pattern *.jpeg
            file_pattern = strcat('*.', typeName);
            D = dir([dirName, file_pattern]);
            Num = length(D(not([D.isdir])));
            ImageName = cell(1,Num);
            for i = 1:Num
                ImageName{i} = strcat(int2str(i), '.', typeName);  
            end
            %{
            % writing a video
            %}
            outputVideo = VideoWriter(fullfile(dirName, VideoName));
            outputVideo.FrameRate = FrameRate;
            open(outputVideo);
            for i = 1:Num
                img = imread(strcat(dirName, ImageName{i}));
                writeVideo(outputVideo,img);
            end
            close(outputVideo);
       end
   end
end