%{
Author: Wen, Hao-KAi
Date: 2013/12/15
Purpose:
test Video Converter
%}
disp('start testing Video Converter');
VC = VideoConverter();
disp('generating testclip:');
disp('clips\testclip\testclip.avi');
VideoConverter.img2video('clips\testclip\','jpeg','testclip.avi', 59.940259);

clear VC;
disp('end testing Video Converter');

