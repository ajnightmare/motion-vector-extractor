%{
Author: Wen, Hao-KAi
Date: 2013/12/16
Purpose:
A renamer renames files in this way:
2.jpeg, 3.jpeg, 7.jpeg, 8.jpeg, 9.jpeg, 12.jpeg, 15.jpeg
=> 1.jpeg, 2.jpeg, 3.jpeg, 4.jpeg, 5.jpeg, 6.jpeg, 7.jpeg
%}
classdef Renamer
    methods(Static)
        %{
        % input
        % dirName: like 'clips\deck_view_performing\'
        % typeName: like 'jpeg'
        %}
        function rename(pathName, typeName)
            %{
            % read all Files with typeName in the folder
            %}
            % file_pattern *.jpeg
            file_pattern = strcat('*.', typeName);
            D = dir([pathName, file_pattern]);
            Num = length(D(not([D.isdir])));
            % find all file
            % some finder numbers may have no file name in the folder.
            finder = 1;
            count = 1;
            while count <= Num
                %{
                % find the file and rename it
                %}
                fileName1 = strcat(int2str(finder), '.', typeName);
                fullName1 = fullfile(pathName, fileName1);
                if exist(fullName1, 'file') == 2
                    % it the file has right index, don't rename it
                    if finder ~= count
                        fullName2 = strcat(int2str(count), '.', typeName);
                        path2 = fullfile(pathName, fullName2);
                        movefile(fullName1, path2);
                    end
                    count = count + 1;
                end
                finder = finder + 1;
            end
        end
    end
end